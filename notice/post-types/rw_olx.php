<?php



/**
 * Registers the `rw_olx` post type.
 */
function rw_olx_init($meta_box) {
	register_post_type(
		'rw_olx',
		[
			'labels'                => [
				'name'                  => __( 'Публікація', 'notice' ),
				'singular_name'         => __( 'Публікація', 'notice' ),
				'all_items'             => __( 'Всі Публікації', 'notice' ),
				'archives'              => __( 'Публікація Archives', 'notice' ),
				'attributes'            => __( 'Публікація Attributes', 'notice' ),
				'insert_into_item'      => __( 'Insert into Публікація', 'notice' ),
				'uploaded_to_this_item' => __( 'Uploaded to this Публікація', 'notice' ),
				'featured_image'        => _x( 'Featured Image', 'rw_olx', 'notice' ),
				'set_featured_image'    => _x( 'Set featured image', 'rw_olx', 'notice' ),
				'remove_featured_image' => _x( 'Remove featured image', 'rw_olx', 'notice' ),
				'use_featured_image'    => _x( 'Use as featured image', 'rw_olx', 'notice' ),
				'filter_items_list'     => __( 'Filter Публікація list', 'notice' ),
				'items_list_navigation' => __( 'Публікація list navigation', 'notice' ),
				'items_list'            => __( 'Публікація list', 'notice' ),
				'new_item'              => __( 'New Публікація', 'notice' ),
				'add_new'               => __( 'Add New', 'notice' ),
				'add_new_item'          => __( 'Add New Публікація', 'notice' ),
				'edit_item'             => __( 'Edit Публікація', 'notice' ),
				'view_item'             => __( 'View Публікація', 'notice' ),
				'view_items'            => __( 'View Публікація', 'notice' ),
				'search_items'          => __( 'Search Публікація', 'notice' ),
				'not_found'             => __( 'No Публікація found', 'notice' ),
				'not_found_in_trash'    => __( 'No Публікація found in trash', 'notice' ),
				'parent_item_colon'     => __( 'Parent Публікація:', 'notice' ),
				'menu_name'             => __( 'Публікація', 'notice' ),
			],
			'public'                => true,
			'hierarchical'          => false,
			'show_ui'               => true,
			'show_in_nav_menus'     => true,
			'supports'              => [ 'title' ],
			'has_archive'           => true,
			'rewrite'               => true,
			'query_var'             => true,
			'menu_position'         => null,
			'menu_icon'             => 'dashicons-admin-post',
			'show_in_rest'          => true,
			'rest_base'             => 'rw_olx',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
		]
	);

}

add_action( 'init', 'rw_olx_init' );

/**
 * Sets the post updated messages for the `rw_olx` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `rw_olx` post type.
 */
function rw_olx_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['rw_olx'] = [
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'RW_OLX updated. <a target="_blank" href="%s">View RW_OLX</a>', 'notice' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'notice' ),
		3  => __( 'Custom field deleted.', 'notice' ),
		4  => __( 'RW_OLX updated.', 'notice' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'RW_OLX restored to revision from %s', 'notice' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false, // phpcs:ignore WordPress.Security.NonceVerification.Recommended
		/* translators: %s: post permalink */
		6  => sprintf( __( 'RW_OLX published. <a href="%s">View RW_OLX</a>', 'notice' ), esc_url( $permalink ) ),
		7  => __( 'RW_OLX saved.', 'notice' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'RW_OLX submitted. <a target="_blank" href="%s">Preview RW_OLX</a>', 'notice' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'RW_OLX scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview RW_OLX</a>', 'notice' ), date_i18n( __( 'M j, Y @ G:i', 'notice' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'RW_OLX draft updated. <a target="_blank" href="%s">Preview RW_OLX</a>', 'notice' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	];

	return $messages;
}

add_filter( 'post_updated_messages', 'rw_olx_updated_messages' );

/**
 * Sets the bulk post updated messages for the `rw_olx` post type.
 *
 * @param  array $bulk_messages Arrays of messages, each keyed by the corresponding post type. Messages are
 *                              keyed with 'updated', 'locked', 'deleted', 'trashed', and 'untrashed'.
 * @param  int[] $bulk_counts   Array of item counts for each message, used to build internationalized strings.
 * @return array Bulk messages for the `rw_olx` post type.
 */
function rw_olx_bulk_updated_messages( $bulk_messages, $bulk_counts ) {
	global $post;

	$bulk_messages['rw_olx'] = [
		/* translators: %s: Number of RW_OLXes. */
		'updated'   => _n( '%s RW_OLX updated.', '%s RW_OLXes updated.', $bulk_counts['updated'], 'notice' ),
		'locked'    => ( 1 === $bulk_counts['locked'] ) ? __( '1 RW_OLX not updated, somebody is editing it.', 'notice' ) :
						/* translators: %s: Number of RW_OLXes. */
						_n( '%s RW_OLX not updated, somebody is editing it.', '%s RW_OLXes not updated, somebody is editing them.', $bulk_counts['locked'], 'notice' ),
		/* translators: %s: Number of RW_OLXes. */
		'deleted'   => _n( '%s RW_OLX permanently deleted.', '%s RW_OLXes permanently deleted.', $bulk_counts['deleted'], 'notice' ),
		/* translators: %s: Number of RW_OLXes. */
		'trashed'   => _n( '%s RW_OLX moved to the Trash.', '%s RW_OLXes moved to the Trash.', $bulk_counts['trashed'], 'notice' ),
		/* translators: %s: Number of RW_OLXes. */
		'untrashed' => _n( '%s RW_OLX restored from the Trash.', '%s RW_OLXes restored from the Trash.', $bulk_counts['untrashed'], 'notice' ),
	];

	return $bulk_messages;
}

add_filter( 'bulk_post_updated_messages', 'rw_olx_bulk_updated_messages', 10, 2 );
