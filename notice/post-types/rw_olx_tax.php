<?php
add_action( 'init', function (){
    $labels = array(
        'name' => 'Тип публікації',
        'singular_name' => 'Тип публікації',
        'menu_name' => 'Тип публікації',
        'all_items' => 'Усі Тип публікації',
        'edit_item' => 'Edit Тип публікації',
        'view_item' => 'View Тип публікації',
        'update_item' => 'Update Тип публікації',
        'add_new_item' => 'Add New Тип публікації',
        'new_item_name' => 'New Тип публікації',
        'parent_item' => 'Parent Тип публікації',
        'parent_item_colon' =>  'Parent Тип публікації',
        'search_items' => 'Search Тип публікації',
        'popular_items' => 'Popular Тип публікації',
        'separate_items_with_commas' => 'Separate Тип публікації with commas',
        'add_or_remove_items' => 'Add or remove Тип публікації',
        'choose_from_most_used' => 'Choose from the most used Тип публікації',
        'not_found' => 'No Тип публікації found.',
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'show_ui' => true,
        'show_in_nav_menus' => true,
        'show_tagcloud' => true,
        'meta_box_cb' => null,
        'show_admin_column' => true,
        'hierarchical' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'rw-olx', 'with_front' => true, 'hierarchical' => true, ),
        'sort' => true,
    );
    register_taxonomy( 'rw_olx', array( 'rw_olx' ) , $args );
});
