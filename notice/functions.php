<?php
/**
 * functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package notice
 */

require_once('vendor/autoload.php');


require_once('post-types/rw_olx.php');
require_once('post-types/rw_olx_tax.php');

use Notice\ThemeFunctions;
use Notice\ThemeConfig;
use Notice\CustomStyle;
use Notice\Notice_Meta_Box;

$style = new CustomStyle();
$themefunc = new ThemeFunctions();
$themeconfig = new ThemeConfig();

$meta_boxes = new Notice_Meta_Box();
add_action( 'add_meta_boxes', [ $meta_boxes, 'add' ] );
add_action( 'save_post', [ $meta_boxes, 'save' ] );



