<?php

namespace Notice;


class ThemeFunctions
{

    /**
     * Output the Breadcrumb.
     *
     * @param array $args Arguments.
     */

    /**
     * Output the Breadcrumb.
     *
     * @param array $args Arguments.
     */



    /**
     * @param string $post_type
     * @param int $posts_per_page
     * @param string $orderby
     * @param string $order
     * @return array
     */


    public function getQueryArgs($post_type = 'post', $posts_per_page = 10, $orderby = 'date', $order = 'DESC' )
    {
        return [
            'post_type' => $post_type,
            'posts_per_page' => $posts_per_page,
            'orderby' => $orderby,
            'order' => $order ,
        ];
    }

    public function getQueryArgsWithPostTax($post_type = 'post',  $posts_per_page = 3, $orderby = 'date', $order = 'DESC', $tax = 'category', $term = 'audition-actor')
    {
        return [
            'post_type' => $post_type,
            'posts_per_page' => $posts_per_page,
            'orderby' => $orderby,
            'order' => $order,
            'tax_query' => [
                [
                    'taxonomy' => $tax,
                    'field'    => 'slug',
                    'terms'    => $term,
                ],
            ],
        ];
    }

    public function getQueryArgsWithTax($post_type = 'post',  $posts_per_page = 2, $orderby = 'date', $order = 'DESC', $tax = 'talent_category', $term = 'actor')
{
    return [
        'post_type' => $post_type,
        'posts_per_page' => $posts_per_page,
        'orderby' => $orderby,
        'order' => $order,
        $tax => $term
    ];
}


    public function trim_content($title, $len = 36 )
    {

        if ( mb_strlen( $title, 'utf-8' ) < 36 ) {
            return $title;
        } else {
            return mb_substr( $title, 0, $len, 'utf-8') . '...';
        }

    }

    public static function postImage( $id, $class = 'post__item-img' )
    {
        if ( $img_url = get_the_post_thumbnail_url( $id ) ) : ?>

            <div class="<?php echo $class ?>-wrap">
                <div class="<?php echo $class ?>">
                    <img class="lazy" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="<?php echo $img_url ?>" alt="<?php self::the_post_thumbnail_alt( $id );?>">
                </div>
            </div>
        <?php endif;

    }


    public static function theImage( $id, $class = 'cards__list-item-img')
    {
        if ( $img_url = get_the_post_thumbnail_url( $id ) ) : ?>

            <span class="<?php echo $class ?>">
							<img class="lazy" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="<?php echo $img_url ?>" alt="<?php self::the_post_thumbnail_alt( $id );?>">
			</span>
        <?php endif;
    }


    public static function singleImage( $id )
    {
        if ( $img_url = get_the_post_thumbnail_url( $id, 'large' ) ) : ?>

            <div class="news-single__content-img-wrap third-top-offset">
                <div class="news-single__content-img">
                    <img class="lazy" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-src="<?php echo $img_url ?>" alt="<?php self::the_post_thumbnail_alt( $id );?>">
                </div>
            </div>
        <?php endif;
    }


    public static function the_post_thumbnail_alt( $post_id )
    {
        $alt = self::get_the_post_thumbnail_alt( $post_id );
        if ( empty( $alt ) ) {
            $alt = get_option( 'blogname' );
        }
        echo $alt;
    }


    public static function get_the_post_thumbnail_alt( $post_id )
    {
        return get_post_meta( get_post_thumbnail_id( $post_id ), '_wp_attachment_image_alt', true);
    }


    function customImageSizes( $image_sizes ){

        // size for slider
        $slider_image_sizes = array( 'talent_image_size' );

        // for ex: $slider_image_sizes = array( 'thumbnail', 'medium' );

        // instead of unset sizes, return your custom size for slider image
        if( isset($_REQUEST['post_id']) && 'talent' === get_post_type( $_REQUEST['post_id'] ) )
//            var_dump($_REQUEST['post']);
//            return $slider_image_sizes;

            return $image_sizes;
    }


    public function getImgHtml($id, $class = 'topics__img pb50')
    {
        if ( $img_url = get_the_post_thumbnail_url() ) : ?>
            <div class="<?php echo $class ?>">
                <img class="lazy" data-src="<?php echo $img_url ?>" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" alt="<?php $this->the_post_thumbnail_alt( $id );?>">
            </div>
        <?php endif;
    }


}