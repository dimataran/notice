<?php

namespace Notice;

class ThemeConfig
{
    const DOMAIN = 'notice';

    public function getDomain()
    {
        return self::DOMAIN;
    }

    public function theDomain()
    {
        echo $this->getDomain();
    }

    public function showTemplate()
    {
        add_action('wp_footer', [$this, 'getTemplate']);
    }

    public function getTemplate()
    {
        global $template;
        print_r($template);
    }


}