<?php

namespace Notice;

class CustomStyle
{

    public $temp_dir;
    public $img_temp_dir;
    public $media_temp_dir;


    public function __construct($img_temp_dir = '/html/public/images/', $media_temp_dir = '/html/public/media/' )
    {
        $this->temp_dir = get_stylesheet_directory_uri();
        $this->img_temp_dir = get_stylesheet_directory_uri() . $img_temp_dir;
        $this->media_temp_dir = get_stylesheet_directory_uri() . $media_temp_dir;

        add_action( 'wp_enqueue_scripts', array($this, 'addStyle') );
    }

    public function getMediaPath()
    {
        return $this->media_temp_dir;
    }

    public function theMediaPath()
    {
        echo $this->media_temp_dir;
    }

    public function getImgPath()
    {
        return $this->img_temp_dir;
    }

    public function theImgPath()
    {
        echo $this->img_temp_dir;
    }

    public function loadJS($js_paths, $path = '', $pref = '')
    {
        if (  $path === '' ){
            $path = $this->temp_dir . '/html/public/js';
        }
        if($pref !== '' ){
            $pref = '-' . $pref;
        }

        foreach ($js_paths as $js_path) {
            wp_enqueue_script($js_path . $pref,  $path . '/' . $js_path . '.js', [], false, true);
        }
    }

    public function addStyle()
    {
        //CSS
        $css_paths = ['index'];
        foreach ($css_paths as $css_path) {
            wp_enqueue_style($css_path,  $this->temp_dir . '/html/public/styles/' . $css_path . '.css', array());
        }

        //JS
        $this->loadJS(['runtime', 'es2015-polyfills', 'index' ]);

    }


}