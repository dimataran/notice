<?php return array(
    'root' => array(
        'name' => 'izumi/izumi-it-company',
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'reference' => '3bd9cbbe45ee505a0160457d1bcd7518da6e314c',
        'type' => 'wordpress-theme',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'izumi/izumi-it-company' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => '3bd9cbbe45ee505a0160457d1bcd7518da6e314c',
            'type' => 'wordpress-theme',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
